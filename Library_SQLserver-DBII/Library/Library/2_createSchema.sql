-- create schema of Library database


-- in order to create schema, switch to the Library database
-- add a statement that specifies the script runs in the context of the Library database



use Library;
go

-- create schema
/*

syntex:
 create  object_type object_name
create schema schema_name authorization owner_name 
*/

-- create sales, humanResources, and production schema
create schema Member authorization dbo;
go

create schema Operation authorization dbo;
go


