package C_Utils;

import M_Entity.Account;
import M_Entity.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.swing.*;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class BookControl {


    private static SessionFactory factory;
    private static Session session;
    private static Transaction transaction;
    private static List<Book> bookList = null;


    public BookControl() {
        factory = new Configuration().
                configure("MySQL_hibernate.cfg.xml").
                addAnnotatedClass(Book.class).
                buildSessionFactory();
    }


        public List<Book> ShowAllBooks() {
        session = factory.openSession();
        transaction = session.beginTransaction();

        //Refill booList
        if(bookList != null){bookList.clear();}
        bookList = session.createQuery("FROM Book").list();

        transaction.commit();
        session.close();
        return bookList;
    }

    public boolean BookBorrowOrReturn(boolean isBorrow, String titleOrbarCode){

            ShowAllBooks();
            if (bookList == null){

                return  false;
            }
        Iterator iterator = bookList.iterator();
            while(iterator.hasNext()){
                Book b = (Book) iterator.next();

                if(b.getTitle().equals(titleOrbarCode) || String.valueOf(b.getBarCode()).equals(titleOrbarCode)){

                    //Check isBorrow or Return;
                    if (isBorrow){

                        //Here is for Borrow

                        //At Book side, handle FK
                        b.setAccount(AccountControl.getCurrentAccount());
                        b.setLoaned(true);

                        //At Account side, maintain the book list for current account
                        AccountControl.getCurrentAccount().getAccountWithBookList().add(b);

                    }else {

                        // Here is for Return

                        //At Book side, handle FK
                        b.setAccount(null);
                        b.setLoaned(false);

                        //At Account side, maintain the book list for current account
                        AccountControl.getCurrentAccount().getAccountWithBookList().remove(b);

                    }
                    session = factory.openSession();
                    transaction = session.beginTransaction();
                    session.update(b);
                    transaction.commit();
                    session.close();
                    return true;
                }
            }

            return false;
    }

    public static List<Book> ShowAllofMyBooks(){

      Account account = new Account();
      return account.getAccountWithBookList();
    }

    public static boolean ReFillBookListForCurrentAccount(){

        if (AccountControl.getCurrentAccount() != null){

            //Clear stale book reference from bookList for current account
            AccountControl.getCurrentAccount().getAccountWithBookList().clear();

            //Get fresh bookList
            new BookControl().ShowAllBooks();

            //Current Account ID
            int currentID = AccountControl.getCurrentAccount().getAccountId();

            if( bookList != null){

                for(Book b : bookList){

                    //Need to check each book's account is null or Not null
                    // because book's account can be null if nobody borrow this book!
                  if (b.getAccount()!=null && currentID == b.getAccount().getAccountId()){
                      //Refill bookList for current account
                      AccountControl.getCurrentAccount().getAccountWithBookList().add(b);
                  }
                }

                return true;
            }
        }

        return false;
    }

}
