import C_Utils.AccountControl;
import V_Window.MainWindow;



public class Main {

    private static void start_program(){

        AccountControl accountControl = new AccountControl();

        MainWindow mainWindow  = new MainWindow();
        mainWindow.setResizable(false);
        mainWindow.setVisible(true);

    }

    public static void main(String[] args) {

        start_program();
    }
}
