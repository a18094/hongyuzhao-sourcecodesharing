package M_Entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table (name = "Author")
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "authorId")
    private int authorId;

    @Column(name = "authorName")
    private String authorName;

    @Column(name = "authorURL")
    private String authorURL;

//    @OneToMany(mappedBy = "Author", targetEntity = Book.class, cascade = CascadeType.ALL)
//    private List<Book> booListForAuthor;

    public Author() {
    }



    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorURL() {
        return authorURL;
    }

    public void setAuthorURL(String authorURL) {
        this.authorURL = authorURL;
    }

//    public List<Book> getBooListForAuthor() {
//        return booListForAuthor;
//    }
//
//    public void setBooListForAuthor(List<Book> booListForAuthor) {
//        this.booListForAuthor = booListForAuthor;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return getAuthorName().equals(author.getAuthorName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAuthorName());
    }
}

