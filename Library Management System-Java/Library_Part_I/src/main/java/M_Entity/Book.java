package M_Entity;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "Book")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "bookId")
    private int bookId;

    @Column (name = "Title")
    private String Title;

    @Column (name = "Subject")
    private String Subject;

    @Column (name = "Category")
    private String Category;



//    @Column (name = "PublishDate")
//    @Temporal(value = TemporalType.DATE)
//    private Date PublishDate;

    @Column (name = "barCode")
    private int barCode; //Random integers - 6 digital

    @Column (name = "isLoaned")
    Boolean isLoaned;


    @ManyToOne
    @JoinColumn(name = "accountId", nullable = true)
    private Account account;

//    @ManyToOne
//    @JoinColumn(name = "authorId")
//    private Author author;

//    @ManyToOne
//    @JoinColumn (name = "bookCategoryId", nullable = true)
//    private BookCategory bookCategory;

    //Default constructor
    public Book() {
    }

    public Book(String title, String subject, int barCode,String category, Boolean isLoaned) {
        this.Title = title;
        this.Subject = subject;
        this.barCode = barCode;
        this.Category = category;
        this.isLoaned = isLoaned;
    }

    //Setter and Getter
    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        this.Category = category;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        this.Title = title;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        this.Subject = subject;
    }

//    public Date getPublishDate() {
//        return PublishDate;
//    }
//
//    public void setPublishDate(Date publishDate) {
//        this.PublishDate = publishDate;
//    }

    public int getBarCode() {
        return barCode;
    }

    public void setBarCode(int barCode) {
        this.barCode = barCode;
    }

    public Boolean getLoaned() {
        return isLoaned;
    }

    public void setLoaned(Boolean loaned) {
        this.isLoaned = loaned;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

//    public Author getAuthor() {
//        return author;
//    }
//
//    public void setAuthor(Author author) {
//        this.author = author;
//    }
//


//    public BookCategory getBookCategory() {
//        return bookCategory;
//    }
//
//    public void setBookCategory(BookCategory bookCategory) {
//        this.bookCategory = bookCategory;
//    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return getTitle().equals(book.getTitle()) &&
                getSubject().equals(book.getSubject());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getSubject());
    }
}
