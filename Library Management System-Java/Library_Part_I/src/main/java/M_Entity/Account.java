package M_Entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "Account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "accountId")
    private int accountId;


    @Column (name = "loginAccount")
    private String loginAccount;

    @Column (name = "Password")
    private String Password;

    @Column (name = "Name")
    private String Name;

    @Column (name = "Address")
    private String Address;

    @Column (name = "Phone")
    private String Phone;

    @Column (name = "isMember")
    private Boolean isMember; //True is member, False is admin

    //Have to change the fetch model from LAZY to EAGER, otherwise
    //program will get runtime errors
    @OneToMany(mappedBy = "account", fetch = FetchType.EAGER)
    private List<Book> accountWithBookList;


    //Default Constructor
    public Account() {
    }

    public Account(String loginAccount, String password, String name, String address, String phone, Boolean isMember) {
        this.loginAccount = loginAccount;
        this.Password = password;
        this.Name = name;
        this.Address = address;
        this.Phone = phone;
        this.isMember = isMember;
    }

    //Setter and Getter
    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getLoginAccount() {
        return loginAccount;
    }

    public void setLoginAccount(String loginAccount) {
        this.loginAccount = loginAccount;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public Boolean getMember() {
        return isMember;
    }

    public void setMember(Boolean member) {
        isMember = member;
    }

    public List<Book> getAccountWithBookList() {
        return accountWithBookList;
    }

    public void setAccountWithBookList(List<Book> accountWithBookList) {
        this.accountWithBookList = accountWithBookList;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return getLoginAccount().equals(account.getLoginAccount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLoginAccount());
    }




}
