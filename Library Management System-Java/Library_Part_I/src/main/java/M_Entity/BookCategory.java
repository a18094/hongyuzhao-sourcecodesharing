package M_Entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table (name = "BookCategory")
public class BookCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "bookCategoryId")
    private int bookCategoryId;

    @Column (name = "categoryName")
    private String categoryName;

    @OneToMany (mappedBy = "BookCategory", targetEntity = Book.class, cascade = CascadeType.ALL)
    private List<Book> bookListForCategory;

    public BookCategory() {
    }

    public int getBookCategoryId() {
        return bookCategoryId;
    }

    public void setBookCategoryId(int bookCategoryId) {
        this.bookCategoryId = bookCategoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<Book> getBookListForCategory() {
        return bookListForCategory;
    }

    public void setBookListForCategory(List<Book> bookListForCategory) {
        this.bookListForCategory = bookListForCategory;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookCategory that = (BookCategory) o;
        return getCategoryName().equals(that.getCategoryName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCategoryName());
    }
}
