<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Welcome to Hongyu Web Service Project</title>
	</head>
	
	<body>
		<f:view>
		Passenger -  Hongyu Zhao 
		<hr></hr>		

		<a href="${pageContext.request.contextPath}/api/passenger">[1]Show list of all Passenger</a>
		<br></br>
		
		<a href="${pageContext.request.contextPath}/api/passenger/0">[2]Show just one Passenger by id</a>
		<br></br>
				
				
		<a href="${pageContext.request.contextPath}/api/passenger/find/family">[3]Find a Passenger by family </a>
		<br></br>
		
		<a href="${pageContext.request.contextPath}/api/passenger/city/n">[4]Show list of Passenger based on the destination city</a>
		<br></br>
		
		
		<a href="${pageContext.request.contextPath}/api/passenger/sorted/family">[5]Show list of Passenger ordered by family</a>
		<br></br>
		
		
		<a href="${pageContext.request.contextPath}/api/passenger/departuredate/1560">[6]Show list of all passenger by departure date </a>
		<br></br>
		
		<a href="${pageContext.request.contextPath}/api/passenger/find/date/city/cityname">[7]Show list of Passenger by departure date and destination city</a>
		<br></br>
		
		<a href="${pageContext.request.contextPath}/api/passenger/find/date/city/cityname">[8]create an index page test all links.</a>
		<br></br>
		
		<a href="${pageContext.request.contextPath}/api/passenger/delete/">[9]delete passenger by id</a>
		<br></br>
		
		

		
		</f:view>
	</body>
	
</html>