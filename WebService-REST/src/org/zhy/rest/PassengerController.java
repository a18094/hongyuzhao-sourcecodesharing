package org.zhy.rest;

import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.zhy.entity.*;
import org.zhy.exception.*;

@RestController
@RequestMapping("/api")
public class PassengerController {

	private final int MAX_PASSENGERS = 10;
	private List<Passenger> passengerList;

	//Init passengerList
	// with @PostConstruct passengerList Just need to initialize only one time
	@PostConstruct
	public void initPassengerList() throws Exception   {

		passengerList = new ArrayList<Passenger>();



		for (int i = 0; i < MAX_PASSENGERS; i++) {
			Passenger c = new Passenger();
			PaymentType p = new PaymentType();
			AddressType a = new AddressType();
			FlightType flight = new FlightType();
			FromType from = new FromType();
			ToType to = new ToType();
			
			
			//1- Generate Payment
			if (((int) (Math.random() * 100)) % 2 == 1) {
				p.setVisa("visa: " + Integer.toString(new Random().nextInt(9000000) + 1000000));	
			}else {
				
				p.setMaster("Master: " + Integer.toString(new Random().nextInt(9000000) + 1000000));
			}
			
			
			//2- Generate Address
			String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			
			//setCity
			char charX = str.charAt((int)(Math.random() * 51));
			char charY = str.charAt((int)(Math.random() * 51));
			a.setCity( Character.toString(charX) + Character.toString(charY));
			
			//setCountry
			a.setCountry("Country" + Integer.toString(new Random().nextInt(900000) + 100000));
			
			//setStreat Number
			a.setNumber((int) (Math.random() * 100));
			
			//set state
			char c1 = str.charAt((int)(Math.random() * 51));
			char c2 = str.charAt((int)(Math.random() * 51));
			a.setState( Character.toString(c1) + Character.toString(c2));
			
			//set Streat Address
			a.setStreetAddress("StreatAddress" +Integer.toString(new Random().nextInt(900000) + 100000));
			
			//
			a.setZipcode("zipCode" + Integer.toString(new Random().nextInt(900000) + 100000));
		
			//3 - Generate From
			from.setDepartureDate(getRandomDateTime());
			from.setDepartureTime(getRandomDateTime());
			
			
			//4 - Generate To
			to.setDestinationDate(getRandomDateTime());
			to.setDestinationTime(getRandomDateTime());
		
			//5 - set flight
			flight.setFrom(from);
			flight.setTo(to);
			
			// 6 - set flight Type
			a.setFlight(flight);
			
			
		

			// 7- Generate Passenger 	
			//setBirthday
			XMLGregorianCalendar randomDate = DatatypeFactory.newInstance()
		            .newXMLGregorianCalendar(
		                   (GregorianCalendar)GregorianCalendar.
		                   getInstance(TimeZone.
		                		   getTimeZone(ZoneOffset.UTC)
		                		   )
		                   );			
			//c.setBirthdate(randomDate);
			c.setBirthdate(getRandomDateTime());
			
			//setFamily
			c.setFamily("Family" + Integer.toString(new Random().nextInt(100) + 100));
			
			//setGender
		
			if (((int)(Math.random() * 10)) % 2 == 1) {
				c.setGender(Gender.F);
				
			}else {
				
				c.setGender(Gender.M);
					
			}
				
			c.setPhone(Integer.toString(new Random().nextInt(900000) + 100000));
		
			//setID
			c.setId(i);
			
			//setName
			c.setName("Passenger" +  Integer.toString(((int) (Math.random() * 100))));
			
			//set passenger payment
			c.setPayment(p);
			
			//set passenger Address
			c.setAddress(a);
			
			//Add object c to passengerList
			passengerList.add(c);
		}

	}
	
	
	public static XMLGregorianCalendar getRandomDateTime() throws Exception {
		
		XMLGregorianCalendar randomDate = DatatypeFactory.newInstance()
	            .newXMLGregorianCalendar(
	                   (GregorianCalendar)GregorianCalendar.
	                   getInstance(TimeZone.
	                		   getTimeZone(ZoneOffset.UTC)
	                		   )
	                   );
		
		return randomDate;
				
	}

	//[1] Show list of all passenger
	@GetMapping("/passenger")
	public List<Passenger> getPassengers() {
		
		//System.out.println(passengerList);
		return passengerList;
	}
	
	//[2] Show passenger by ID (0,1,2,3 ....)
	@GetMapping("/passenger/{passengerID}")
	public Passenger getPassengerByID(@PathVariable int passengerID) {		
	
		return passengerList.get(passengerID);
	}
	
	
	//[3]Find a passenger by family
	@GetMapping("/passenger/find/{familyName}")
	public List<Passenger> getPassengersByFamily(@PathVariable String familyName ) {

		
		//Sort city for each passenger
		return passengerList.stream().
				filter(c->c.getFamily().toLowerCase().contains(familyName.toLowerCase())).
				collect(Collectors.toList());
				
	}

	
	
	
	//[4]
	@GetMapping("/passenger/city/{cityname}")
	public List<Passenger> getPassengersByCity(@PathVariable String cityname ) {

		
		//get Passenger by City
		return passengerList.stream().
				filter(c->c.getAddress().getCity().toLowerCase().contains(cityname.toLowerCase())).
						collect(Collectors.toList());	
	
				
	}

	
	//[5]Show list of passenger ordered by family
	@GetMapping("/passenger/sorted/family")
	public List<Passenger> getPassengersSortedByFamily() {

		//Sort city for each passenger
		return passengerList.stream().sorted((c1,c2)->c1.getFamily().compareTo(c2.getFamily())).collect(Collectors.toList());	

	}


	
	//[6]
	@GetMapping("/passenger/departuredate/{depdate}")
	public List<Passenger> getPassengersByDepTime(@PathVariable String depdate ) {

		
		
		
		//get Passenger by City
		return passengerList.stream().
				filter(c->c.getAddress().getFlight().getFrom().getDepartureDate().toString().contains(depdate.toString())).
						collect(Collectors.toList());	
	
				
	}


	
	//[8]
	
	//[9] delete by ID
	@DeleteMapping("/passenger/delete/{pID}")
	public List<Passenger> RemovebyID(@PathVariable int  pID ) {
		
		
		
		Passenger pp = new Passenger ();
		
		if (pp.getId() == (pID) ) {
			
			passengerList.remove(pp);
		}			
			
			return passengerList;	

		
	}
	
	
}
