﻿using ISMS_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ISMS_CompanyUI_Dev
{
    /// <summary>
    /// Interaction logic for ActivityView.xaml
    /// </summary>
    public partial class ActivityView : Window
    {
        Database db = new Database();
        List<Activities> actList = new List<Activities>();
        public ActivityView()
        {
            InitializeComponent();
            BindDataToListView();
        }


        //List<Activities> actList = new List<Activities>();
        public void BindDataToListView() {

            actList = db.GetAllActivities();

            actlv.ItemsSource = actList;
            actlv.Items.Refresh();

        }



        private void ActViewClose_Click(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch { }
        }

        private void actLv_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
