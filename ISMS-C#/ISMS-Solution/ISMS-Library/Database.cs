﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ISMS_Library.Requests;

namespace ISMS_Library
{

    public class Database
    {
        // DBSTR_AZURE

        public const string DBSTR_ZHY = @"Data Source=isms-ipd16.database.windows.net;Initial Catalog=ISMS-IPD16;User ID=sqladmin;Password=A1s2d3f4;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";


        // DBSTR_ZHY :  ZHY PC db connection string
        //public const string DBSTR_ZHY = @"Data Source=OWNER-PC\SQLEXPRESSZHY;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        // DBSTR_JiaLing


        //Connect DB in constructor: Database()
        private SqlConnection conn;
        public Database()
        {
            conn = new SqlConnection(DBSTR_ZHY);
            conn.Open();
        }

        //TODO:  Add different CRUD methods here 

        //1. Verify login account for employess or parents or students
        // Call:  int x = LoginVerify("username", "password", "tableName");

        public int LoginVerification(string empName, string empPWD, string tblName)
        {
            SqlCommand cmdSelect = null;
            switch (tblName.ToLower())
            {

                case "tblemployees":
                    cmdSelect = new SqlCommand("SELECT eID from tblemployees  where eAccount=@account And ePassword=@password ", conn);
                    break;
                case "tblparents":
                    cmdSelect = new SqlCommand("SELECT pID from tblparents  where pAccount=@account And pPassword=@password ", conn);
                    break;
                case "tblstudents":
                    cmdSelect = new SqlCommand("SELECT sID from tblstudents  where sAccount=@account And sPassword=@password ", conn);
                    break;
                default:
                    break;

            }

            int id = 0;
            cmdSelect.Parameters.AddWithValue("@account", empName);
            cmdSelect.Parameters.AddWithValue("@password", empPWD);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    //MessageBox.Show("eid: " + eid);
                    id = int.Parse(reader[0].ToString());
                    // MessageBox.Show(eid.ToString());

                }
            }
            return id;
        }

        // 2. GetAllActivities()
        public List<Activities> GetAllActivities()
        {
            List<Activities> list = new List<Activities>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM tblactivities ORDER BY actPubDate DESC",conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int actid = int.Parse(reader[0].ToString());
                    int actmaxnums = int.Parse(reader[7].ToString());
                    int actregisterednums = int.Parse(reader[8].ToString());

                    string acttheme = (string)reader[1];
                    string actcontent = (string)reader[2];
                    string actplace = (string)reader[5];
                    string actcostperperson = (string)reader[6];


                    DateTime actpubdate = (DateTime)reader[3];
                    DateTime actactdate = (DateTime)reader[4];

                    //Assign all data read from table into a instance of Class Activities
                    list.Add(new Activities() { actID = actid, actPubDate = actpubdate, actTheme = acttheme, actContent = actcontent, actActDate = actactdate, actPlace = actplace, CostPerPerson = actcostperperson, actMaxNums = actmaxnums, actRegisteredNums = actregisterednums });
                }
            }
            return list;
        }

        //3. GetAllParentStudentName()
        public List<String> GetAllParentStudentName()
        {

            List<string> nameStrList = new List<string>();

            //SqlCommand cmdSelect = new SqlCommand("select pName,p.pid,sName,s.sid from tblparents as P  inner join tblstudents as S on P.pid = S.pid", conn);

            SqlCommand cmdSelect = new SqlCommand("select pName,p.pid,sName,s.sid from tblparents as P  inner join tblstudents as S on P.pid = S.pid union select pName, p.pid, sName, s.sid from tblparents as P  right join tblstudents as S on P.pid = S.pid", conn);

            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    string strLine = "";

                    if (reader[0].ToString() == "" || reader[1].ToString() == "")
                    {

                        strLine = "NoParents" + ';' + "NoPID" + ';' + reader[2].ToString() + ';' + reader[3].ToString();

                    }
                    else
                    {

                        strLine = reader[0].ToString() + ';' + reader[1].ToString() + ';' + reader[2].ToString() + ';' + reader[3].ToString();
                    }

                    nameStrList.Add(strLine);
                }
            }

            return nameStrList;
        }

        // 4. GetShownRequest() to get all data in request tables
        public List<Requests> GetShownRequest()
        {
            //int replycount;
            List<Requests> list = new List<Requests>();

            //read data from both request and replies
            //Read all data from table request           
            SqlCommand cmdRequestSelect = new SqlCommand("SELECT * FROM tblrequests WHERE pID = 2 ORDER BY reqDate DESC;", conn);
           

            //store the data from the table request
            using (SqlDataReader reader = cmdRequestSelect.ExecuteReader())
            {               
                while (reader.Read())
                {                   
                    //Receive data from the table requrests
                    int reqid = int.Parse(reader[0].ToString());
                    int parentid = int.Parse(reader[1].ToString());
                    //int studentid = int.Parse(reader[2].ToString());    //not used, keep it for future

                    string reqtype = (string)reader[3];
                    string reqsubject = (string)reader[4];
                    string reqcontent = (string)reader[5];
                    string reqstatus = (string)reader[7];

                    DateTime reqdate = (DateTime)reader[6];

                    //Secondly read the table tblrequestreplies to get numbers of replies for given request

                    //read the table tblrequestreplies to get count the sum 
                    SqlCommand cmdReplyCount = new SqlCommand("select count(*) from tblrequestreplies where reqID = @reqID; ", conn);
                    cmdReplyCount.Parameters.AddWithValue("@reqID", reqid);
                    //int replyCountGetStatus = (int)cmdReplyCount.ExecuteNonQuery();
                                 
                    //using (SqlDataReader replyCountReader = cmdReplyCount.ExecuteReader())
                    //{
                    //    if (replyCountReader.Read())
                    //    {
                    //        replycount = int.Parse(replyCountReader[0].ToString());
                    //    }
                    //    else
                    //    {
                    //        throw new Exception("Error occurs when reading reply count!");
                    //    }

                    //}
                    
                    //Assign all data read from table into a instance of Class Activities
                    list.Add(item: new Requests() { ID = reqid, rDate = reqdate, subject = reqsubject, Content = reqcontent});
                }
            }
            return list;
        }


        //5. GetAllReuqests()
        public List<RequestsForListView> GetAllReuqests()
        {
            List<RequestsForListView> list = new List<RequestsForListView>();
  
            SqlCommand cmdSelectAllRequests = new SqlCommand("SELECT * FROM tblrequests", conn);
 
            using (SqlDataReader reader = cmdSelectAllRequests.ExecuteReader())
            {
                while (reader.Read())
                {
                    

                    //Receive data from the table requrests
                    int reqid = int.Parse(reader[0].ToString());

                    //pID or sID from tblrequests
                    int pID, sID;
                    if (!String.IsNullOrEmpty(reader[1].ToString()))
                    {

                        pID = int.Parse(reader[1].ToString());

                    }
                    else {

                        pID = 0; // means no pID found!
                    }

                    if (! String.IsNullOrEmpty (reader[2].ToString()))
                    {
                       

                        sID = int.Parse(reader[2].ToString());

                    }
                    else
                    {
                        sID = 0; // Means no sID found



                    }

                    string reqtype = reader[3].ToString();
                    string reqsubject = reader[4].ToString();
                    string reqcontent = reader[5].ToString();
                    //DateTime reqdate = (DateTime)reader[6];
                    string reqdate = reader[6].ToString();
                    string reqstatus = reader[7].ToString();

                    list.Add(new RequestsForListView(reqid, reqtype, reqsubject, reqcontent, reqdate, reqstatus,pID,sID));

                }
            }
            return list ;
           
        }

        //6. GetAllReplies()
        public List<RequestsReplies> GetAllRepliesOnGivenRequestID(int reqID)
        {
            List<RequestsReplies> list = new List<RequestsReplies>();

            //read data from both request and replies
            //Read all data from table request           
           SqlCommand cmdReplySelect = new SqlCommand("SELECT * FROM tblrequestreplies WHERE reqID = @reqID ORDER BY issuedDate DESC;", conn);
            //SqlCommand cmdReplySelect = new SqlCommand("SELECT * FROM tblrequestreplies WHERE reqID = @reqID;", conn);
            cmdReplySelect.Parameters.AddWithValue("@reqID", reqID);

            //store the data from the table request
            using (SqlDataReader reader = cmdReplySelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    //Receive data from the table requrests
                    int replyid = int.Parse(reader[0].ToString());
                    int reqid = int.Parse(reader[1].ToString());
                    //int eid= int.Parse(reader[2].ToString());

                    string replycontent = (string)reader[3];

                    DateTime issueddate;
                    if (reader[4] == null)
                    {
                        issueddate = DateTime.Now;
                    }
                    else
                    {
                        DateTime.TryParse(reader[4].ToString(), out issueddate);
                    }                       
                    
                    //Assign all data read from table into a instance of Class Reply
                    list.Add(new RequestsReplies() {replyID = replyid,  reqID = reqid, /*eID = eid,*/ replyContent = replycontent, issuedDate = issueddate });
                }
            }
            return list;
        }

        //7.InserAct()
        public int InserAct(newActivity act)
        {
            SqlCommand cmdInsert = new SqlCommand(
                "INSERT INTO tblactivities(actTheme,actContent, actCostPerPerson,actPlace, actActDate,actPubDate, actMaxNums,actRegisteredNums) " +
                "OUTPUT INSERTED.actID " +
                "VALUES (@actTheme,@actContent,@actCostPerPerson,@actPlace,@actActDate,@actPubDate,@actMaxNums,@actRegisteredNums)", conn);
            cmdInsert.Parameters.AddWithValue("@actTheme", act.actTheme);
            cmdInsert.Parameters.AddWithValue("@actContent", act.actContent);
            cmdInsert.Parameters.AddWithValue("@actCostPerPerson", act.CostPerPerson);
            cmdInsert.Parameters.AddWithValue("@actPlace", act.actPlace);
            cmdInsert.Parameters.AddWithValue("@actActDate", act.actActDate);
            cmdInsert.Parameters.AddWithValue("@actPubDate", act.actPubDate);
            cmdInsert.Parameters.AddWithValue("@actMaxNums", act.actMaxNums);
            cmdInsert.Parameters.AddWithValue("@actRegisteredNums", act.actRegisteredNums);

            //Error:  ExecuteScalar  ==> get “ Specified cast is not valid ” error
            //Fixed:1. object tmp = cmdInsert.ExecuteScalar();
            //      2. Convert tmp other data type
            object tmp = cmdInsert.ExecuteScalar();
            int returnID = int.Parse(tmp.ToString());
            return returnID;
          
        }

        //8.InsertReply
        public int InsertReply(RequestsReplies reply)
        {
            //DateTime currentDate = DateTime.Today.Date;
            SqlCommand cmdInsert = new SqlCommand(
                "INSERT INTO tblrequestreplies(reqID,eID, replyContent, issuedDate) " +
                "OUTPUT INSERTED.replyID " +
                "VALUES (@reqID,@eID,@replyContent,@repliedDate)", conn);
            cmdInsert.Parameters.AddWithValue("@reqID", reply.reqID);
            cmdInsert.Parameters.AddWithValue("@eID", reply.eID);
            cmdInsert.Parameters.AddWithValue("@replyContent", reply.replyContent);
            cmdInsert.Parameters.AddWithValue("@repliedDate", reply.issuedDate);

            //Error:  ExecuteScalar  ==> get “ Specified cast is not valid ” error
            //Fixed:1. object tmp = cmdInsert.ExecuteScalar();
            //      2. Convert tmp other data type
            object tmp = cmdInsert.ExecuteScalar();
            int returnID = int.Parse(tmp.ToString());
            return returnID;

        }

        //8-2. InsertNewReply()
        public int InsertNewReply(RequestsReplies item)
        {
            SqlCommand cmdInsert = new SqlCommand(
                "INSERT INTO tblrequestreplies (reqID,replyContent,issuedDate)" +
                "OUTPUT INSERTED.replyID " +
                "VALUES (@reqID, @replyContent,@issuedDate)", conn);
            cmdInsert.Parameters.AddWithValue("@reqID", item.reqID);
            //cmdInsert.Parameters.AddWithValue("@eID", 100); //Tempo set as 100
            cmdInsert.Parameters.AddWithValue("@replyContent", item.replyContent);
            cmdInsert.Parameters.AddWithValue("@issuedDate", item.issuedDate);

            //Error:  ExecuteScalar  ==> get “ Specified cast is not valid ” error
            //Fixed:1. object tmp = cmdInsert.ExecuteScalar();
            //      2. Convert tmp other data type
            object tmp = cmdInsert.ExecuteScalar();
            int returnID = int.Parse(tmp.ToString());
            return returnID;
        }


        // 9. getParentAccountInfo()
        public List<Parents> getParentAccountInfo() {

            List<Parents> list = new List<Parents>();

            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM tblparents", conn);

            //store the data from the table request
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    //Receive data from the table requrests
                    int id = int.Parse(reader[0].ToString());
                    string account = reader[1].ToString();
                    string name = (string)reader[3];
                    string phone = (string)reader[4];

                    list.Add(new Parents() {ID=id,Account=account,Name=name,Phone=phone});
                }
            }
            return list;

    }

        // 10. getEmpAccountInfo()
        public List<Employees> getEmpAccountInfo()
        {

            List<Employees> list = new List<Employees>();

            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM tblEmployees", conn);

            //store the data from the table request
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    //Receive data from the table requrests
                    int id = int.Parse(reader[0].ToString());
                    string account = reader[1].ToString();
                    string name = (string)reader[3];
                    string phone = (string)reader[4];

                    list.Add(new Employees() { ID = id, Account = account, Name = name, Phone = phone });
                }
            }
            return list;

        }

        // 11. getStuAccountInfo()
        public List<Students> getStuAccountInfo()
        {

            List<Students> list = new List<Students>();

            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM tblStudents", conn);

            //store the data from the table request
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    //Receive data from the table requrests
                    int id = int.Parse(reader[0].ToString());
                    string account = reader[2].ToString();
                    string name = (string)reader[4];
                    string phone = (string)reader[5];

                    list.Add(new Students() { ID = id, Account = account, Name = name, Phone = phone });
                }
            }
            return list;

        }


        // 12 InsertUpdateAccount(string tblname, People p, string txtswitch)
        string FieldAccount, FieldPassword, FieldName, FieldPhone, FieldAddress, FieldEmail, FieldSwitch; //FieldpID
        public int InsertUpdateAccount(string tblname, People p, string txtswitch, int parentID=0)
        {
            //string FieldAccountName;
            //check table name
            switch (tblname.ToLower()) {
                case "tblparents":
                    FieldAccount = "pAccount";
                    FieldPassword = "pPassword";
                    FieldName = "pName";
                    FieldPhone = "pPhone";
                    FieldAddress = "pAddress";
                    FieldEmail = "pEmail";
                    FieldSwitch = "pContractNumber"; // string:  2019123456
                    break;
                case "tblemployees":
                    FieldAccount = "eAccount";
                    FieldPassword = "ePassword";
                    FieldName = "eName";
                    FieldPhone = "ePhone";
                    FieldAddress = "eAddress";
                    FieldEmail = "eEmail";
                    FieldSwitch = "eTitle"; //Employee or Manager
                    break;
                case "tblstudents":
                    FieldAccount = "sAccount";
                    FieldPassword = "sPassword";
                    FieldName = "sName";
                    FieldPhone = "sPhone";
                    FieldAddress = "sAddress";
                    FieldEmail = "sEmail";
                    FieldSwitch = "sPassportNum"; //PassportNo
                    //FieldpID = "pID";
                    break;
                default:
                    break;
            }


            //Check if the people already exist in DB. If No, insert DB. Yes, update DB
            string account = p.Account;
            if (account == "") {

                throw new Exception ("No account infomation fetched from input box");
            }

            string SQLstr = "SELECT * FROM " + tblname + " WHERE " + FieldAccount + "=@account"; // + p.Account;

            SqlCommand cmdSelect = new SqlCommand(SQLstr, conn);
            cmdSelect.Parameters.AddWithValue("@account", account);

            //int insertORupdate = (int)cmdSelect.ExecuteScalar();

            var insertORupdate = cmdSelect.ExecuteScalar();           
            

            string sqlSTR; SqlCommand cmdInsertOrUpdate;
            if (insertORupdate !=null)
            {
                // Update
                //SqlCommand cmdUpdate = new SqlCommand("UPDATE  People SET Name=@name, Age=@age WHERE Id=@id", conn);

                sqlSTR = "UPDATE " + tblname.ToString() + " SET " +
                    FieldAccount + "=@account," +
                    FieldPassword + "=@password," +
                    FieldName + "=@name," +
                    FieldPhone + "=@phone," +
                    FieldAddress + "=@address," +
                    FieldEmail + "=@email," +
                    FieldSwitch + "=@SwitchName" + " WHERE " + FieldName + "=@name2";
                

                cmdInsertOrUpdate = new SqlCommand(sqlSTR, conn);


                cmdInsertOrUpdate.Parameters.AddWithValue("@account", p.Account);
                cmdInsertOrUpdate.Parameters.AddWithValue("@password", p.Password);
                cmdInsertOrUpdate.Parameters.AddWithValue("@name", p.Name);
                cmdInsertOrUpdate.Parameters.AddWithValue("@phone", p.Phone);
                cmdInsertOrUpdate.Parameters.AddWithValue("@address", p.Address);
                cmdInsertOrUpdate.Parameters.AddWithValue("@email", p.Email);
                cmdInsertOrUpdate.Parameters.AddWithValue("@SwitchName", txtswitch.ToString());
                cmdInsertOrUpdate.Parameters.AddWithValue("@name2", p.Name);


            }
            else {
                //Insert

                //SqlCommand cmdInsert = new SqlCommand("INSERT INTO People(Name,Age) OUTPUT INSERTED.ID VALUES(@Name, @Age) ", conn);

                sqlSTR = "INSERT INTO " + tblname.ToString() +
              "(" + FieldAccount + "," +
                    FieldPassword + "," +
                    FieldName + "," +
                    FieldPhone + "," +
                    FieldAddress + "," +
                    FieldEmail + "," +
                    FieldSwitch + ")" + " VALUES " +
                "(@account,@password,@name,@phone,@address,@email,@SwitchName)";

                cmdInsertOrUpdate = new SqlCommand(sqlSTR, conn);

                cmdInsertOrUpdate.Parameters.AddWithValue("@account", p.Account);
                cmdInsertOrUpdate.Parameters.AddWithValue("@password", p.Password);
                cmdInsertOrUpdate.Parameters.AddWithValue("@name", p.Name);
                cmdInsertOrUpdate.Parameters.AddWithValue("@phone", p.Phone);
                cmdInsertOrUpdate.Parameters.AddWithValue("@address", p.Address);
                cmdInsertOrUpdate.Parameters.AddWithValue("@email", p.Email);
                cmdInsertOrUpdate.Parameters.AddWithValue("@SwitchName", txtswitch.ToString());               

            }

            int result = (int)cmdInsertOrUpdate.ExecuteNonQuery();

            return result;


        }


        //13. InsertNewRequest()
        public int InsertNewRequest(Requests item)
        {
            SqlCommand cmdInsert = new SqlCommand(
                "INSERT INTO tblrequests (pID, reqsubject, reqContent, reqDate)" +
                "OUTPUT INSERTED.reqID " +
                "VALUES (@pID, @reqsubject, @reqContent, @reqDate)", conn);
            cmdInsert.Parameters.AddWithValue("@pID", item.pID);
            //cmdInsert.Parameters.AddWithValue("@eID", 100); //Tempo set as 100
            cmdInsert.Parameters.AddWithValue("@reqsubject", item.subject);
            cmdInsert.Parameters.AddWithValue("@reqContent", item.Content);
            cmdInsert.Parameters.AddWithValue("@reqDate", item.rDate);

            //Error:  ExecuteScalar  ==> get “ Specified cast is not valid ” error
            //Fixed:1. object tmp = cmdInsert.ExecuteScalar();
            //      2. Convert tmp other data type
            object tmp = cmdInsert.ExecuteScalar();
            int returnID = int.Parse(tmp.ToString());
            return returnID;

        }


        //14. GetParentContractNumber()
        public int GetParentContractNumber(int id)
        {
            SqlCommand cmdSelect = new SqlCommand("SELECT pName from tblparents where pID=@ID", conn);
            cmdSelect.Parameters.AddWithValue("@ID", id);

            int ContractNumber = 0;
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    ContractNumber = int.Parse(reader[0].ToString());
                    break;
                }
            }
            return ContractNumber;
        }


        //15. GetParentName()
        public string GetPNameOrEName(int id, string tblName)
        {
            SqlCommand cmdSelect = null;
            switch (tblName.ToLower())
            {

                case "tblemployees":
                    cmdSelect = new SqlCommand("SELECT eName from tblemployees where eID=@ID", conn);
                    break;
                case "tblparents":
                    cmdSelect = new SqlCommand("SELECT pName from tblparents where pID=@ID", conn);
                    break;
                //case "tblstudents":
                //    cmdSelect = new SqlCommand("SELECT sName from tblstudents  where sAccount=@account And sPassword=@password ", conn);
                //    break;
                default:
                   // MessageBox.Show("Error when reading table parent or table employee");
                    break;
            }

            string Name = "";
            cmdSelect.Parameters.AddWithValue("@ID", id);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    //MessageBox.Show("eid: " + eid);
                    Name = reader[0].ToString();
                    break;
                    // MessageBox.Show(eid.ToString());

                }
            }
            return Name;
        }
    }
}
