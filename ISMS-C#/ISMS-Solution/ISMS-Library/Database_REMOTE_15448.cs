﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISMS_Library
{
   
    public class Database
    {
        // DBSTR_AZURE

        public const string DBSTR_ZHY = @"Data Source=isms-ipd16.database.windows.net;Initial Catalog=ISMS-IPD16;User ID=sqladmin;Password=A1s2d3f4;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";


        // DBSTR_ZHY :  ZHY PC db connection string
        //public const string DBSTR_ZHY = @"Data Source=OWNER-PC\SQLEXPRESSZHY;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        // DBSTR_JiaLing


        //Connect DB in constructor: Database()
        private SqlConnection conn;
        public Database() {
            conn = new SqlConnection(DBSTR_ZHY);
            conn.Open();
        }

        //TODO:  Add different CRUD methods here 

        //1. Verify login account for employess or parents or students
        // Call:  int x = LoginVerify("username", "password", "tableName");

        public int LoginVerification(string empName, string empPWD, string tblName)
        {
            SqlCommand cmdSelect=null;
            switch (tblName.ToLower()){

                case "tblemployees":
                    cmdSelect = new SqlCommand("SELECT eID from tblemployees  where eAccount=@account And ePassword=@password ", conn);
                    break;
                case "tblparents":
                    cmdSelect = new SqlCommand("SELECT eID from tblparents  where eAccount=@account And ePassword=@password ", conn);
                    break;
                case "tblstudents":
                    cmdSelect = new SqlCommand("SELECT eID from tblstudents  where eAccount=@account And ePassword=@password ", conn);
                    break;
                default:
                    break;

            }

            int eid=0;
            cmdSelect.Parameters.AddWithValue("@account", empName);
            cmdSelect.Parameters.AddWithValue("@password", empPWD);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    //MessageBox.Show("eid: " + eid);
                    eid = int.Parse(reader[0].ToString());
                    // MessageBox.Show(eid.ToString());

                }
            }
            return eid;
        }

        // 2. GetAllActivities()
        public List<Activities> GetAllActivities()
        {
            List<Activities> list = new List<Activities>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM tblactivities");
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int actid = (int)reader[0];
                    int actmaxnums = (int)reader[7];
                    int actregisterednums = (int)reader[8];

                    string acttheme = (string)reader[1];
                    string actcontent = (string)reader[2];
                    string actplace = (string)reader[5];
                    string actcostperperson = (string)reader[6];
                    

                    DateTime actpubdate = (DateTime)reader[3];
                    DateTime actactdate = (DateTime)reader[4];

                    //Assign all data read from table into a instance of Class Activities
                    list.Add(new Activities() { actID = actid, ActPubDate = actpubdate, actTheme = acttheme, actContent = actcontent, actActDate = actactdate, actPlace = actplace, CostPerPerson = actcostperperson, actMaxNums = actmaxnums, actRegisteredNums = actregisterednums });
                }
            }
            return list;
        }

        //3. GetAllParentStudentName()
        public List<String> GetAllParentStudentName() {

            List<string> nameStrList = new List<string>();
            

            SqlCommand cmdSelect = new SqlCommand("select pName,sName  from tblparents as P  inner join tblstudents as S on P.pid = S.pid", conn);

            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    //MessageBox.Show("eid: " + eid);
                    //eid = int.Parse(reader[0].ToString());
                    // MessageBox.Show(eid.ToString());
                    string strLine = "";
                    strLine = reader[0].ToString() +';'+ reader[1].ToString();
                    nameStrList.Add(strLine);

                }
            }

            return nameStrList;
        }


    }
}
