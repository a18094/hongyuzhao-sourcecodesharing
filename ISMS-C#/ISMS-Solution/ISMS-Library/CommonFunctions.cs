﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ISMS_Library
{
    public static class CommonFunctions
    {
        //Gobal Variables

        //Store logged in account ID
        public static int loggedInAccountID { set; get; }
        public static string loggedInAccount { set; get; }
        public static string loggedInAccountName { set; get; }

        //Sahred data of requests
        public static int requestID { set; get; }
        public static string requestSubject { set; get; }
        public static string requestContent { set; get; }
        public static DateTime requestDate { set; get; }
        public static string requestType { set; get; }
        public static string requestStatus { set; get; }


        //Store Selected Account ID from Treeview
        public static int tvSelectedAccountID { set; get; }

        //Store Selected Account type from Treeview  
        public static string tvSelectedAccountType { set; get; }


        //Selected DateTime from Calender
        public static DateTime selectedDateTime { set; get; }

        //Global Functions
        //1. public string str2md5

        public static string str2md5(string str) {

            byte[] bytePWD = Encoding.Default.GetBytes(str.Trim());
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] output = md5.ComputeHash(bytePWD);
            
            // return md5 string
            return  BitConverter.ToString(output).Replace("-", "");
        }




    }
}
