﻿using ISMS_Library;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ISMS_ParentUI_Dev
{
    
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    
    public partial class Login : Window
    {
        public int LoginTimes = 5;
        public int leftRetryTimes;
         

        public Login()
        {
            InitializeComponent();
            this.ShowDialog();
        }

        private void ParentLogin_BtnClick(object sender, RoutedEventArgs e)
        {
            if (LoginTimes > 1)
            {
                if (txtAccount.Text == "" || txtAccount.Text.Contains(';'))
                {
                    leftRetryTimes = LoginTimes - 1;
                    LoginTimes--;
                    MessageBox.Show("Account is empty or contains semicolo. You still have " + leftRetryTimes + " times to try!");
                    return;
                }

                if (txtPWD.Password.Length < 6 || txtPWD.Password.Length > 20)
                {

                    leftRetryTimes = LoginTimes - 1;
                    LoginTimes--;
                    MessageBox.Show("Password must be between 6-20 characters long!  You still have " + leftRetryTimes + " times to try!");
                    return;
                }

                try
                {
                    Database Db = new Database();
                    // Convert password to MD5 String
                    string md5PWD = CommonFunctions.str2md5(txtPWD.Password);

                    //Verify login account and password in database
                    int pid = Db.LoginVerification(txtAccount.Text.ToString(), md5PWD, "tblparents");
                    if (pid > 0)
                    {
                        //MessageBox.Show("Login successfully, ID is " + eid.ToString());
                        //Login successfully. Hide login Window
                        CommonFunctions.loggedInAccountID = pid;
                        
                        this.Hide(); //Hide login window
                        
                    }
                    else
                    {
                        leftRetryTimes = LoginTimes - 1;
                        LoginTimes--;
                        MessageBox.Show("Account or Password authentication failed! You still have " + leftRetryTimes + " times to try!");
                        return;
                    }
                }
                catch (Exception ex)
                {

                    if (ex is EncoderFallbackException || ex is InvalidOperationException
                        || ex is ObjectDisposedException || ex is ArgumentNullException || ex is SqlException)

                    {

                        MessageBox.Show("Encrypt password errors! " + ex.Message, "ISMS Message Box", MessageBoxButton.OK, MessageBoxImage.Error);

                        Environment.Exit(0);

                    }
                }

            }
            else
            {

                //TODO: exit if login retry more than 5 times!          

                MessageBox.Show("Your login failed  too many times. Program exit!", "ISMS Message Box", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(0);
            }
        }

        private void ParentExitLogin_BtnClick(object sender, RoutedEventArgs e)
        {
            MessageBoxResult msgResult = MessageBox.Show("Do you want to quit ISMS program?", "ISMS Message Box", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (msgResult == MessageBoxResult.Yes)
            {
                Environment.Exit(0);
            }
            else
            { return; }
        }
    }
}
