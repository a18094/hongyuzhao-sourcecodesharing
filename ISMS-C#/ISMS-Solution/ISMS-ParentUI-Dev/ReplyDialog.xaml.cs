﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ISMS_Library;
using MahApps.Metro.Controls;

namespace ISMS_ParentUI_Dev
{
    /// <summary>
    /// Interaction logic for ReplyDialog.xaml
    /// </summary>
    public partial class ReplyDialog
    {
        List<Requests> requestOneShow = new List<Requests>();
        List<RequestsReplies> repliesList = new List<RequestsReplies>();
        

        public ReplyDialog(Window owner)
        {
            InitializeComponent();
            Owner = owner;

            /////////// Show one request content per ParentID ////////////////////////

            lvShowOneRequest.ItemsSource = requestOneShow;

            //Requests item = new Requests();
            //item.ID = CommonFunctions.requestID;
            //item.subject = CommonFunctions.requestSubject;
            //item.Content = CommonFunctions.requestContent;
            //item.rDate = CommonFunctions.requestDate;
            //item.reqType = CommonFunctions.requestType;
            //item.reqStatus = CommonFunctions.requestStatus;


            //requestOneShow.Add(item);

            requestOneShow.Add(item: new Requests() { ID = 2, subject = "resend bi-weekly report to me", Content = "resend bi-weekly report to me", rDate = DateTime.Now, reqType = "Important", reqStatus = "good" });

            //MessageBox.Show(CommonFunctions.loggedInAccountID.ToString());

            requestOneShow.Add(item: new Requests() { ID = 2, subject = "kk", Content = "ll", rDate = DateTime.Now, reqType = "Important", reqStatus = "good" });

            requestOneShow.ToString();

            lvShowOneRequest.Items.Refresh();

            /////////// Show replieList per ParentID ////////////////////////
            List<string> replyShowList = new List<string>();
            lvReplyList.ItemsSource = replyShowList;

            Database db = new Database();
            repliesList = db.GetAllRepliesOnGivenRequestID(/*CommonFunctions.requestID*/2);
            foreach (RequestsReplies item in repliesList)
            {
                replyShowList.Add(item.ShowReplyList());
            }
            

            lvReplyList.Items.Refresh();
        }

        private void BtnSubmitReply_Click(object sender, RoutedEventArgs e)
        {                              
            RequestsReplies newreply = new RequestsReplies();
            newreply.reqID = 2;
            //newreply.reqID = CommonFunctions.requestID;
            newreply.replyContent = tbNewReplyContent.Text;
            newreply.issuedDate = DateTime.Now;
            Database db = new Database();

            int returnFlag = db.InsertNewReply(newreply);

            if (returnFlag > 0)
            {
                MessageBox.Show("Insert new reply in database successfully!");
                //Clean current window after insert successful
                tbNewReplyContent.Text = "";

                repliesList.Clear();
                repliesList = db.GetAllRepliesOnGivenRequestID(2);
                repliesList.ToString();

                lvReplyList.Items.Refresh();
                return;
            }
        }
    }
}
