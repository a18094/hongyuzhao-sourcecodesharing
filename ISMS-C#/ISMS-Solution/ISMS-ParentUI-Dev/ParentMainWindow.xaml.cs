﻿using ISMS_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data.SqlClient;


namespace ISMS_ParentUI_Dev
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ParentMainWindow : MetroWindow
    {
        List<ActivitiesShowShort> activityShow = new List<ActivitiesShowShort>();
        List<RequestsShowShort> requestShow = new List<RequestsShowShort>();
        //string ParentName;
        //int ContractNumber;

        public ParentMainWindow()
        {
            InitializeComponent();
            
            LoadList();

            lvActivitiesShow.ItemsSource = activityShow;
            lvRequestShow.ItemsSource = requestShow;
          
            lvActivitiesShow.Items.Refresh();
            lvRequestShow.Items.Refresh();

            //tbParentName.Text = ParentName.ToString();     //有问题
            //lbParentName.Content = ParentName.ToString();  //有问题

            //lbContractNumber.Content = ContractNumber;  //有问题
        }

        public void LoadList()  //Load the lvActivities and lvRequest-Reply List, Read data of Activities, Request, AccountName, AccountContractNumber
        {
            Database Db = new Database(); //Open net connection

            ///////////////         read the data in Table Activity           ////////////////////////

            activityShow.Clear();
            List<Activities> activityMiddle = new List<Activities>();
            
            int _id;
            string _theme;
            DateTime _pubdate;

            //read the data in Table Request

            //transfer data in Table Activity to activityList
           
            try
            {
                activityMiddle = Db.GetAllActivities();

                foreach (Activities item in activityMiddle)
                {
                    _id = item.actID;
                    _theme = item.actTheme;
                    _pubdate = item.actPubDate;

                    ActivitiesShowShort media = new ActivitiesShowShort(_id, _theme, _pubdate);
                    activityShow.Add(media);
                }
                activityShow.ToString();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message,
                    "Table Activities of ISMS Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            /////////// Read data in Table Requests ////////////////////////
            
            requestShow.Clear();
            List<Requests> requestMiddle = new List<Requests>();

            int _reqid;
            string _reqsubject;
            DateTime _reqissueddate;
            //int _replycount;

            try
            {
                //read data 
                
                requestMiddle = Db.GetShownRequest(/*CommonFunctions.loggedInAccountID*/);    

                foreach (Requests item in requestMiddle)
                {
                    _reqid = item.ID;
                    _reqsubject = item.subject;
                    _reqissueddate = item.rDate;
                    //_replycount = item.replyCount;

                    RequestsShowShort reqmedia = new RequestsShowShort(_reqid, _reqsubject, _reqissueddate);
                    requestShow.Add(reqmedia);
                }
                requestShow.ToString();

            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message,
                    "Table Request of ISMS Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            ////////////////////  Read Account Name /////////////////
            //string ParentName;
            //try
            //{
            //    ParentName = Db.GetPNameOrEName(CommonFunctions.loggedInAccountID, "tblparents");
            //    //lbParentName.Content = "Zhou Xingxing";
            //    MessageBox.Show(CommonFunctions.loggedInAccountID.ToString());
            //    //tbParentName.Text = ParentName.ToString();     
            //    lbParentName.Content = ParentName.ToString();
            //}
            //catch (SqlException ex)
            //{
            //    MessageBox.Show("Error executing SQL query:\n" + ex.Message,
            //        "Table Request of ISMS Database", MessageBoxButton.OK, MessageBoxImage.Error);
            //}

            ////////////////////  Read ContractNumber /////////////////
            //int ContractNumber;
            //try
            //{
            //    ContractNumber = Db.GetParentContractNumber(CommonFunctions.loggedInAccountID);
            //    lbContractNumber.Content = "123456";
            //    //lbContractNumber.Content = ContractNumber;  //有问题
            //}
            //catch (SqlException ex)
            //{
            //    MessageBox.Show("Error executing SQL query:\n" + ex.Message,
            //        "Table Request of ISMS Database", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
        }

        private void ExpandReqReplyList_ButtonClick(object sender, RoutedEventArgs e)
        {

            ReplyDialog replydialog = new ReplyDialog(this);
            if (replydialog.ShowDialog() == true)
            {
                // MessageBox.Show("Good!");
            }
        }

        private void Profile_ButtonClick(object sender, RoutedEventArgs e)
        {
            
            Profile AddRequest = new Profile(this);
            if (AddRequest.ShowDialog() == true)
            {
                // MessageBox.Show("Good!");
            }
        }

        private void OpenItem_ContexMenuClick(object sender, RoutedEventArgs e)
        {
            RequestsShowShort currRequestItem = lvRequestShow.SelectedItem as RequestsShowShort;

            if (currRequestItem == null)
            {
                return;
            }

            //CommonFunctions.requestID = currRequestItem.ID;
            //CommonFunctions.requestSubject = currRequestItem.subject;
            //CommonFunctions.requestContent = currRequestItem.Content;
            //CommonFunctions.requestDate = currRequestItem.rDate;
            //CommonFunctions.requestType = currRequestItem.reqType;
            //CommonFunctions.requestStatus = currRequestItem.reqStatus;

            try
            {
                ReplyDialog replydialog = new ReplyDialog(this);
                if (replydialog.ShowDialog() == true)
                {
                    // MessageBox.Show("Good!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error", ex.Message);
            }

        }
    }
}
