USE [master]
GO
/****** Object:  Database [ISMS-IPD16]    Script Date: 5/13/2019 12:30:53 PM ******/
CREATE DATABASE [ISMS-IPD16]
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ISMS-IPD16].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ISMS-IPD16] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET ARITHABORT OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ISMS-IPD16] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [ISMS-IPD16] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ISMS-IPD16] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [ISMS-IPD16] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET  MULTI_USER 
GO
ALTER DATABASE [ISMS-IPD16] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ISMS-IPD16] SET ENCRYPTION ON
GO
ALTER DATABASE [ISMS-IPD16] SET QUERY_STORE = ON
GO
ALTER DATABASE [ISMS-IPD16] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 100, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO)
GO
USE [ISMS-IPD16]
GO
/****** Object:  Table [dbo].[tblactivities]    Script Date: 5/13/2019 12:30:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblactivities](
	[actID] [smallint] IDENTITY(1,1) NOT NULL,
	[actTheme] [nvarchar](100) NOT NULL,
	[actContent] [nvarchar](500) NULL,
	[actPubDate] [datetime] NOT NULL,
	[actActDate] [datetime] NOT NULL,
	[actPlace] [nvarchar](200) NOT NULL,
	[actCostPerPerson] [nvarchar](50) NOT NULL,
	[actMaxNums] [smallint] NOT NULL,
	[actRegisteredNums] [smallint] NOT NULL,
 CONSTRAINT [pk_tblactivities] PRIMARY KEY CLUSTERED 
(
	[actID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblactstudents]    Script Date: 5/13/2019 12:30:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblactstudents](
	[actstuID] [smallint] IDENTITY(1,1) NOT NULL,
	[actID] [smallint] NOT NULL,
	[sID] [smallint] NOT NULL,
 CONSTRAINT [pk_tblactstudents] PRIMARY KEY CLUSTERED 
(
	[actstuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblemployees]    Script Date: 5/13/2019 12:30:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblemployees](
	[eID] [smallint] IDENTITY(1,1) NOT NULL,
	[eAccount] [nvarchar](40) NOT NULL,
	[ePassword] [nvarchar](100) NOT NULL,
	[eName] [nvarchar](40) NOT NULL,
	[eTitle] [nvarchar](20) NOT NULL,
	[ePhone] [nvarchar](20) NOT NULL,
	[eAddress] [nvarchar](100) NOT NULL,
	[eEmail] [nvarchar](40) NOT NULL,
 CONSTRAINT [pk_tblemployees] PRIMARY KEY CLUSTERED 
(
	[eID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblparents]    Script Date: 5/13/2019 12:30:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblparents](
	[pID] [smallint] IDENTITY(1,1) NOT NULL,
	[pAccount] [nvarchar](40) NOT NULL,
	[pPassword] [nvarchar](100) NOT NULL,
	[pName] [nvarchar](40) NOT NULL,
	[pPhone] [nvarchar](20) NOT NULL,
	[pAddress] [nvarchar](100) NOT NULL,
	[pEmail] [nvarchar](40) NOT NULL,
	[pContractNumber] [nvarchar](20) NULL,
 CONSTRAINT [pk_tblparents] PRIMARY KEY CLUSTERED 
(
	[pID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblreports]    Script Date: 5/13/2019 12:30:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblreports](
	[rptID] [smallint] IDENTITY(1,1) NOT NULL,
	[rptType] [nvarchar](20) NOT NULL,
	[rptSubject] [nvarchar](50) NOT NULL,
	[rptContent] [nvarchar](2000) NOT NULL,
	[rptDate] [datetime] NOT NULL,
	[reportTo] [smallint] NOT NULL,
 CONSTRAINT [pk_tblreports] PRIMARY KEY CLUSTERED 
(
	[rptID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblrequestreplies]    Script Date: 5/13/2019 12:30:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblrequestreplies](
	[replyID] [int] IDENTITY(1,1) NOT NULL,
	[reqID] [int] NOT NULL,
	[eID] [smallint] NULL,
	[replyContent] [nvarchar](2000) NOT NULL,
	[issuedDate] [datetime] NULL,
 CONSTRAINT [pk_tblrequestreplies] PRIMARY KEY CLUSTERED 
(
	[replyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblrequests]    Script Date: 5/13/2019 12:30:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblrequests](
	[reqID] [int] IDENTITY(1,1) NOT NULL,
	[pID] [smallint] NULL,
	[sID] [smallint] NULL,
	[reqType] [nvarchar](20) NOT NULL,
	[reqsubject] [nvarchar](50) NOT NULL,
	[reqContent] [nvarchar](2000) NOT NULL,
	[reqDate] [datetime] NOT NULL,
	[reqStatus] [nvarchar](20) NOT NULL,
 CONSTRAINT [pk_tblrequests] PRIMARY KEY CLUSTERED 
(
	[reqID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblstudents]    Script Date: 5/13/2019 12:30:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblstudents](
	[sID] [smallint] IDENTITY(1,1) NOT NULL,
	[pID] [smallint] NULL,
	[sAccount] [nvarchar](40) NOT NULL,
	[sPassword] [nvarchar](100) NOT NULL,
	[sName] [nvarchar](40) NOT NULL,
	[sPhone] [nvarchar](20) NOT NULL,
	[sAddress] [nvarchar](100) NOT NULL,
	[sEmail] [nvarchar](40) NOT NULL,
	[sPassportNum] [nvarchar](20) NOT NULL,
 CONSTRAINT [pk_tblstudents] PRIMARY KEY CLUSTERED 
(
	[sID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblactstudents]  WITH CHECK ADD  CONSTRAINT [fk_actTOactstudents] FOREIGN KEY([actID])
REFERENCES [dbo].[tblactivities] ([actID])
GO
ALTER TABLE [dbo].[tblactstudents] CHECK CONSTRAINT [fk_actTOactstudents]
GO
ALTER TABLE [dbo].[tblactstudents]  WITH CHECK ADD  CONSTRAINT [fk_stuTOactstudents] FOREIGN KEY([sID])
REFERENCES [dbo].[tblstudents] ([sID])
GO
ALTER TABLE [dbo].[tblactstudents] CHECK CONSTRAINT [fk_stuTOactstudents]
GO
ALTER TABLE [dbo].[tblreports]  WITH CHECK ADD  CONSTRAINT [fk_reportToParents] FOREIGN KEY([reportTo])
REFERENCES [dbo].[tblparents] ([pID])
GO
ALTER TABLE [dbo].[tblreports] CHECK CONSTRAINT [fk_reportToParents]
GO
ALTER TABLE [dbo].[tblrequestreplies]  WITH CHECK ADD  CONSTRAINT [fk_empToreply] FOREIGN KEY([eID])
REFERENCES [dbo].[tblemployees] ([eID])
GO
ALTER TABLE [dbo].[tblrequestreplies] CHECK CONSTRAINT [fk_empToreply]
GO
ALTER TABLE [dbo].[tblrequestreplies]  WITH CHECK ADD  CONSTRAINT [fk_requestToreply] FOREIGN KEY([reqID])
REFERENCES [dbo].[tblrequests] ([reqID])
GO
ALTER TABLE [dbo].[tblrequestreplies] CHECK CONSTRAINT [fk_requestToreply]
GO
ALTER TABLE [dbo].[tblrequests]  WITH CHECK ADD  CONSTRAINT [fk_parentsTOreqquest] FOREIGN KEY([pID])
REFERENCES [dbo].[tblparents] ([pID])
GO
ALTER TABLE [dbo].[tblrequests] CHECK CONSTRAINT [fk_parentsTOreqquest]
GO
ALTER TABLE [dbo].[tblrequests]  WITH CHECK ADD  CONSTRAINT [fk_studentsTOrequests] FOREIGN KEY([sID])
REFERENCES [dbo].[tblstudents] ([sID])
GO
ALTER TABLE [dbo].[tblrequests] CHECK CONSTRAINT [fk_studentsTOrequests]
GO
ALTER TABLE [dbo].[tblstudents]  WITH CHECK ADD  CONSTRAINT [fk_parentTOstudents] FOREIGN KEY([pID])
REFERENCES [dbo].[tblparents] ([pID])
GO
ALTER TABLE [dbo].[tblstudents] CHECK CONSTRAINT [fk_parentTOstudents]
GO
ALTER TABLE [dbo].[tblemployees]  WITH CHECK ADD  CONSTRAINT [CK_eTitle] CHECK  (([eTitle]='Employee' OR [eTitle]='Manager'))
GO
ALTER TABLE [dbo].[tblemployees] CHECK CONSTRAINT [CK_eTitle]
GO
ALTER TABLE [dbo].[tblreports]  WITH CHECK ADD  CONSTRAINT [CK_rptype] CHECK  (([rptType]='Other' OR [rptType]='Monthly' OR [rptType]='BiWeekly' OR [rptType]='Weekly'))
GO
ALTER TABLE [dbo].[tblreports] CHECK CONSTRAINT [CK_rptype]
GO
ALTER TABLE [dbo].[tblrequests]  WITH CHECK ADD  CONSTRAINT [CK_reqStatus] CHECK  (([reqStatus]='Finished' OR [reqStatus]='Pending' OR [reqStatus]='Waiting' OR [reqStatus]='InProgress'))
GO
ALTER TABLE [dbo].[tblrequests] CHECK CONSTRAINT [CK_reqStatus]
GO
ALTER TABLE [dbo].[tblrequests]  WITH CHECK ADD  CONSTRAINT [CK_reqType] CHECK  (([reqType]='Normal' OR [reqType]='Reminder' OR [reqType]='Notice' OR [reqType]='Important'))
GO
ALTER TABLE [dbo].[tblrequests] CHECK CONSTRAINT [CK_reqType]
GO
USE [master]
GO
ALTER DATABASE [ISMS-IPD16] SET  READ_WRITE 
GO
